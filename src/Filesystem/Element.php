<?php

namespace Mars\Filesystem;

class Element
{
    /**
     * @var string $path
     */
    protected $path;

    /**
     * @var resource $handle
     */
    protected $handle;

    /**
     * @var bool $isOpen;
     */
    protected $isOpen;

    /**
     * @param string $path
     */
    public function __construct(string $path)
    {
        $this->path = $path;
    }

    /**
     * @return bool
     */
    public function readable()
    {
        return is_readable($this->path);
    }

    /**
     * @return array
     */
    public function pathinfo()
    {
        return pathinfo($this->path);
    }
    
    /**
     * @return bool
     */
    public function isOpen()
    {
        return $this->isOpen;
    }

    /**
     * @return string
     */
    public function getDir()
    {
        return dirname($this->path);
    }

    public function ensureIsOpen()
    {
        if (!$this->isOpen())
            throw new Exception("File is not open");
    }

    public function ensureExists()
    {
        if (!$this->exists())
            throw new Exception("File does not exist");
    }
}