<?php

namespace Mars\Filesystem;

use Mars\Debug\Exception;
use Mars\Filesystem\Element;
use Mars\Filesystem\ElementInterface;

class File extends Element implements ElementInterface
{
    /**
     * @return bool
     */
    public function exists()
    {
        return file_exists($this->path);
    }

    /**
     * @return string
     */
    public function getContent()
    {
        $this->ensureExists();

        $this->open("r");
        $content = $this->read();
        $this->close();

        return $content;
    }

    /**
     * @param string $mode
     * 
     * @return File
     */
    public function open(string $mode)
    {
        $this->handle = fopen($this->path, $mode);

        $this->isOpen = true;

        return $this;
    }

    /**
     * @return string
     */
    public function read()
    {
        $this->ensureIsOpen();

        return fread($this->handle);
    }

    /**
     * @param string $content
     * 
     * @return File
     */
    public function write(string $content)
    {
        $this->ensureIsOpen();

        fwrite($this->handle, $content);
    }

    /**
     * @return File
     */
    public function close()
    {
        $this->ensureIsOpen();

        fclose($this->handle);

        $this->handle = null;
        $this->isOpen = false;

        return $this;
    }

    /**
     * @param bool $shared Shared lock
     * 
     * @return File
     */
    public function lock(bool $shared = false)
    {
        $this->ensureIsOpen();

        flock($this->handle, $shared ? LOCK_SH : LOCK_EX);

        return $this;
    }

    /**
     * @return File
     */
    public function unlock()
    {
        $this->ensureIsOpen();

        flock($this->handle, LOCK_UN);

        return $this;
    }

    /**
     * @return int
     */
    public function size()
    {
        $this->ensureExists();

        return filesize($this->path);
    }
}