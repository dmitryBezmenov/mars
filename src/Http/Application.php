<?php

namespace Mars\Http;

use Mars\Http\Request;
use Mars\Http\Response;
use Mars\Kernel\Container;
use Mars\Kernel\KernelException;
use Mars\Config\File as Config;

class Application
{
    /**
     * @var string $basePath
     */
    protected $basePath;

    /**
     * @var Container $context
     */
    protected $context;

    /**
     * @var Config $config
     */
    protected $config;

    /**
     * @param string $basePath   Root project path
     * @param string $configPath Main configuration file
     */
    public function __construct(string $basePath, string $configPath)
    {
        $this->basePath = $basePath;

        $this->context = new Container();

        $this->config = new Config($basePath . "/" . ltrim($configPath, "/"));        
    }

    /**
     * 
     */
    public function handle(Request $request)
    {
        $response = new Response();
    }
}