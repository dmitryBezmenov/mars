<?php

namespace Mars\Config;

use Mars\Filesystem\File as FilesystemFile;
use Mars\Debug\Exception;
use Mars\Support\Traits\DotAccess;

class File
{
    use DotAccess;

    /**
     * @var FilesystemFile $file
     */
    protected $file;

    /**
     * @param string $path
     */
    public function __construct(string $path)
    {
        $this->file = new File($path);

        if (!$this->exists())
            throw new Exception("Configuration file not found at $path");

        $this->loadValues();
    }

    /**
     * Loading values in format like a basic version of yaml (only basic types of values)
     */
    private function loadValues()
    {
        $content = $this->file->getContent();
        
        $context = [];
        $tabs = 0;

        foreach (explode(PHP_EOL, $content) as $i => $line)
        {
            if ("" == trim($line))
                continue;

            [$name, $value] = explode(":", $line);

            $indent = strspn($name, '\t');

            if ($indent > $tabs)
            {
                if ($indent - $tabs > 0)
                    throw new Exception("Invalid configuration file: so many indent at {$this->path}:" . $i + 1);
            }
            else
            {
                for ($i = $indent - 1; $i < $tabs; $i++)
                {
                    array_pop($context);
                }
            }

            $context[] = ltrim($name, '\t');

            if (in_array($value, ["true", "false"]))
            {
                $value = ($value == "true");
            }
            
            $this->set(join(".", $context), $value);
        }
    }
}