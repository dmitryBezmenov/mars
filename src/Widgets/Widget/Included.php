<?php

namespace Mars\Widgets\Widget;

class Included
{
    /**
     * @var string $content
     */
    protected $content;

    /**
     * @param string $content
     */
    public function __construct(string $content)
    {
        $this->content = $content;
    }

    public function show()
    {
        echo $this->content;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }
}