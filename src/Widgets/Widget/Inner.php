<?php

namespace Mars\Widgets\Widget;

class Inner
{
    /**
     * @var string $content
     */
    protected $content;

    /**
     * @param mixed $content
     */
    public function __construct($content)
    {
        if (is_array($content))
        {
            $content = array_reduce($content, function($result, $part) 
            {
                if (!is_string($part))
                    throw new \Exception("Bad inner content type");

                return $result .= $part;
            }, "");
        }
        else if (!is_string($content))
            throw new \Exception("Bad inner content type");

        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }
}