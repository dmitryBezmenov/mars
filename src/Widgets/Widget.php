<?php

namespace Mars\Widgets;

use Mars\Widgets\Widget\Included;
use Mars\Widgets\Widget\Inner;

class Widget
{
    /**
     * @var string $path
     */
    protected $name;

    /**
     * @var string $theme
     */
    protected $theme;

    /**
     * @var string $dir
     */
    protected $dir;

    /**
     * @param string $name
     * @param string $dir
     */
    public function __construct(string $name, string $dir)
    {
        if (empty($name))
            throw new \Exception("Invalid widget name");

        $this->name = $name;
        $this->dir = $dir;
    }

    public function setTheme(string $theme)
    {
        if (empty($theme))
            throw new \Exception("Invalid widget theme");

        $this->theme = $theme;
    }

    /**
     * @param array  $params
     * @param string $inner
     * 
     * @return bool
     */
    public function include(array $params = [], string $inner = "")
    {
        /**
         * The theme is handled as a sub-widget
         */
        if ($this->theme)
        {
            $themeWidget = new Widget("{$this->name}.{$this->theme}", $this->dir);

            if (!$themeWidget->exists())
                throw new \Exception("Widget ({$this->name}) theme ({$this->theme}) does not exists");
        }

        /**
         * Get body of theme template if it exists
         */
        if ($this->theme && $themeWidget->templateExists())
            $result = $themeWidget->getBody($params);
        else
            $result = $this->getBody($params);

        $result = str_replace(
            "##BODY##", $body, 
            $this->getBody($params)
        );

        /**
         * Include styles
         */
        if ($this->theme && $themeWidget->cssExists())
            $result .= $themeWidget->getCss();
        else if ($this->cssExists())
            $result .= $this->getCss();

        /**
         * Include javascript
         */
        if ($this->theme && $themeWidget->jsExists())
            $result .= $themeWidget->getJs($params);
        else if ($this->jsExists())
            $result .= $this->getJs($params);

        return new Included($result);
    }

    /**
     * @return string
     */
    public function getPath()
    {
        $basePath = 
            $_SERVER['DOCUMENT_ROOT'] . "/" 
            . ltrim($this->dir, "/") . "/" 
            . str_replace(".", DIRECTORY_SEPARATOR, $this->name);

        if ($this->theme)
            return $basePath . "/" . $this->theme;
        else
            return $basePath;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        $basePath = 
            ($_SERVER['SERVER_PORT'] == "443" ? "https" : "http") 
            . "://" . $_SERVER["HTTP_HOST"]
            . "/" . str_replace(".", "/", $this->name);

        if ($this->theme)
            return $basePath . "/" . $this->theme;
        else
            return $basePath;
    }

    /**
     * @return string
     */
    public function getTemplate(array $params = [])
    {
        ob_start();
        require $this->getPath();
        return ob_get_clean();
    }

    /**
     * @param array  $params
     * 
     * @return string
     */
    public function getJs(array $params = [])
    {
        $script = file_get_contents($this->getJsPath());
        $params = json_encode($params);

        return 
        "<script>
            let params = {$params};   
            {$script}
        </script>"
        ;
    }

    /**
     * @return string
     */
    public function getCss()
    {
        return "<link rel='stylescheet' href='{$this->getCssUrl()}'>";
    }

    /**
     * @return string
     */
    public function getTemplatePath()
    {
        return $this->getPath() . "/widget.php";
    }

    /**
     * @return string
     */
    public function getCssPath()
    {
        return $this->getPath() . "/widget.css";
    }

    /**
     * @return string
     */
    public function getCssUrl()
    {
        return $this->getUrl() . "/widget.css";
    }

    /**
     * @return string
     */
    public function getJsPath()
    {
        return $this->getPath() . "/widget.js";
    }

    /**
     * @return string
     */
    public function getJsUrl()
    {
        return $this->getUrl() . "/widget.js";
    }

    /**
     * @return bool
     */
    public function exists()
    {
        return file_exists($this->getPath());
    }

    /**
     * @return bool
     */
    public function templateExists()
    {
        return file_exists($this->getTemplatePath());
    }

    /**
     * @return bool
     */
    public function cssExists()
    {
        return file_exists($this->getCssPath());
    }

    /**
     * @return bool
     */
    public function jsExists()
    {
        return file_exists($this->getJsPath());
    }
}