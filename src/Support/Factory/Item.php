<?php

namespace Mars\Support\Factory;

interface Item
{
    public function isValidFactoryItem(...$params);
}