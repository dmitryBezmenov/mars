<?php

namespace Mars\Support;

use Mars\Debug\Exception;
use Mars\Support\Factory\Item as FactoryItem;

class Factory
{
    /**
     * @var array $container
     */
    protected $container;

    /**
     * @param array $container 
     */
    public function __construct(array $container = [])
    {
        $this->validate($container);

        $this->container = $container;
    }

    /**
     * @param array|string $additional
     */
    public function add($additional)
    {
        $additional = (array)$additional;

        $this->validate($additional);

        $this->container = array_merge($this->container, $additional);
    }

    /**
     * Match class by parameters
     * 
     * @return string
     */
    public function search(...$params)
    {
        foreach ($this->container as $class)
        {
            if ($class::isValidFactoryItem(...$params))
                return $class;
        }

        return false;
    }

    /**
     * Check is container valid
     */
    private function validate(array $container)
    {
        foreach ($container as $item)
        {
            if (!is_string($item))
                throw new Exception("Only class can be binded in factory");

            if (!($item instanceof FactoryItem))
                throw new Exception("Factory-bound classes must the implement Mars\\Support\\Factory\\Item interface");
        }
    }
}