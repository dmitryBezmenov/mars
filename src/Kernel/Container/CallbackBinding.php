<?php

namespace Mars\Kernel\Container;

use Mars\Kernel\Container\Binding;

class CallbackBinding implements Binding
{
    /**
     * @var callable $binding;
     */
    protected $binding;

    /**
     * @param string $binding
     */
    public function __construct(callable $binding)
    {
        $this->binding = $binding;
    }

    public static function isValidFactoryItem($binding)
    {
        return is_callable($binding);
    }

    public function get(...$params)
    {
        return $this->binding(...$params);
    }
}