<?php

namespace Mars\Kernel\Container;

use Mars\Support\Factory\Item as FactoryItem;

interface Binding extends FactoryItem
{
    public function get(...$params);
}